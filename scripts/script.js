$(document).mousemove( function(e) {
  $('#winterA').parallax(50 , e);
  $('#winterB').parallax(70 , e);
  $('#winterC').parallax(90 , e);
  $('#winterD').parallax(110 , e);
  $('#winterE').parallax(130 , e);
  $('#winterF').parallax(150 , e);
  $('#winterAnimals').parallax(170 , e);
  $('#winterRiver').parallax(190 , e);

  $('#summerA').parallax(50 , e);
  $('#summerB').parallax(70 , e);
  $('#summerC').parallax(90 , e);
  $('#summerD').parallax(110 , e);
  $('#summerE').parallax(130 , e);
  $('#summerF').parallax(150 , e);

  $('[data-name="springA"]').parallax(50 , e);
  $('[data-name="springB"]').parallax(70 , e);
  $('[data-name="springC"]').parallax(70 , e);
  $('[data-name="springD"]').parallax(90 , e);
  $('[data-name="IE8p6l.tif"]').parallax(90 , e);
  $('[data-name="VIeZFd.tif"]').parallax(110 , e);
  $('[data-name="JAjmRd.tif"]').parallax(110 , e);
  $('[data-name="springF"]').parallax(130 , e);
  $('[data-name="springA"]').parallax(150 , e);
  $('[data-name="springA"]').parallax(170 , e);
  $('[data-name="springA"]').parallax(190 , e);

  $('[data-name="autumnA"]').parallax(50 , e);
  $('[data-name="montagne"]').parallax(70 , e);
  $('[data-name="autumnAnimaux"]').parallax(70 , e);
  $('[data-name="autumnB"]').parallax(90 , e);
  $('[data-name="autumnC"]').parallax(90 , e);
  $('[data-name="autumnD"]').parallax(110 , e);
  $('[data-name="autumnMaison"]').parallax(110 , e);
  $('[data-name="autumnE"]').parallax(130 , e);
  $('[data-name="autumnF"]').parallax(150 , e);
  $('[data-name="autumnG"]').parallax(170 , e);
  $('[data-name="autumnH"]').parallax(190 , e);

  
  
});

// Parallax mouse
$.fn.parallax = function (resistance, mouse){
  $el = $(this);
  TweenLite.to($el, 0.2,
  {
    x : -(( mouse.clientX - (window.innerWidth*1) ) / resistance ),
    y : -(( mouse.clientY - (window.innerHeight*1) ) / resistance )
  });
};

$('a[href^="#"]').click(function(){
  var the_id = $(this).attr("href");

  $('html, body').animate({
    scrollTop:$(the_id).offset().top
  }, 'slow');
  return false;
});


$('.loader')
  .delay(2900)
  .queue(function (next) { 
    $(this).css('display', 'none'); 
    next(); 
});
































